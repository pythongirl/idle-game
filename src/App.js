import React, { Component } from 'react'
import './App.css'

class App extends Component {
  render() {
    return (
      <div className="app">
        <div class="header">This is some header text</div>
        <div class="column-container">
          <div class="column column-left">Left</div>
          <div class="column column-center">Center</div>
          <div class="column column-right">Right</div>
        </div>
        <div class="footer">This is the footer</div>
      </div>
    )
  }
}

export default App
